<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{
    /**
     * @Route("/nastenka", name="dashboard")
     */
    public function indexAction()
    {
        return $this->render('@App/Dashboard/index.html.twig');
    }
}
