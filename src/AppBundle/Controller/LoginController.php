<?php

namespace AppBundle\Controller;

use AppBundle\Model\User\Role;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends Controller
{
    /**
     * @Route("/prihlaseni", name="login")
     * @Route("/odhlaseni", name="logout")
     * @param \Symfony\Component\Security\Http\Authentication\AuthenticationUtils $authUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(AuthenticationUtils $authUtils)
    {
        if ($this->isGranted(Role::USER)) {
            return $this->redirectToRoute('dashboard');
        }

        $error = $authUtils->getLastAuthenticationError();
        if ($error !== null && $error->getMessageKey()) {
            $this->addFlash('error', 'Přihlášení se nepovedlo.');
        }

        $lastUsername = $authUtils->getLastUsername();

        return $this->render('@App/Login/index.html.twig', [
            'lastUsername' => $lastUsername,
        ]);
    }
}
