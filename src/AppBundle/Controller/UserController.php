<?php

namespace AppBundle\Controller;

use AppBundle\Model\User\ChangePasswordFormType;
use AppBundle\Model\User\Exception\InvalidUserPasswordException;
use AppBundle\Model\User\UserFacade;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/muj-profil", name="user_my_profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myProfileAction()
    {
        $user = $this->getUser();
        /* @var $user \AppBundle\Model\User\User */

        return $this->render('@App/User/myProfile.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/muj-profil/zmena-hesla", name="user_change_password")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Model\User\UserFacade $userFacade
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction(Request $request, UserFacade $userFacade)
    {
        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $userFacade->changePassword($this->getUser(), $form->getData());
                $this->addFlash('success', 'Vaše heslo bylo úspěšně změněno.');

                return $this->redirectToRoute('user_my_profile');
            } catch (InvalidUserPasswordException $e) {
                $form->get('oldPassword')->addError(new FormError('Staré heslo není správné'));
            }
        }

        return $this->render('@App/User/changePassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
