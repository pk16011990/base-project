<?php

namespace AppBundle\Model\User;

use AppBundle\Model\User\Exception\InvalidUserPasswordException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFacade
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \AppBundle\Model\User\UserRepository
     */
    private $userRepository;

    /**
     * @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param \AppBundle\Model\User\UserRepository $userRepository
     * @param \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(
        EntityManager $em,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $userPasswordEncoder
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param \AppBundle\Model\User\User $user
     * @param \AppBundle\Model\User\SupportFormData $changePasswordData
     */
    public function changePassword(User $user, SupportFormData $changePasswordData)
    {
        if (!$this->userPasswordEncoder->isPasswordValid($user, $changePasswordData->oldPassword)) {
            throw new InvalidUserPasswordException();
        }
        $passwordHash = $this->userPasswordEncoder->encodePassword($user, $changePasswordData->newPassword);

        $user->setPassword($passwordHash);
        $this->em->flush($user);
    }
}
