<?php

namespace AppBundle\Model\User;

use AppBundle\Model\User\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserSecurityProvider implements UserProviderInterface
{

    /**
     * @var \AppBundle\Model\User\UserRepository
     */
    private $userRepository;

    /**
     * @param \AppBundle\Model\User\UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     * @return \AppBundle\Model\User\User
     */
    public function loadUserByUsername($username)
    {
        try {
            return $this->userRepository->getByEmail($username);
        } catch (UserNotFoundException $e) {
            throw new UsernameNotFoundException();
        }
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return \AppBundle\Model\User\User|\Symfony\Component\Security\Core\User\UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException();
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === User::class;
    }
}
