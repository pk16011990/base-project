<?php

namespace AppBundle\Model\User\Exception;

class InvalidUserPasswordException extends \Exception
{
}
