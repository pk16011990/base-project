<?php

namespace AppBundle\Model\User\Exception;

class UserNotFoundException extends \Exception
{
}
