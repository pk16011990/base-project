<?php

namespace AppBundle\Model\User;

class ChangePasswordData
{
    /**
     * @var string|null
     */
    public $oldPassword;

    /**
     * @var string|null
     */
    public $newPassword;
}
