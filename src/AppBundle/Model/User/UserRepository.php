<?php

namespace AppBundle\Model\User;

use AppBundle\Model\User\Exception\UserNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class UserRepository
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getUserRepository(): EntityRepository
    {
        return $this->em->getRepository(User::class);
    }

    /**
     * @param int $userId
     * @return \AppBundle\Model\User\User|null
     */
    public function getById(int $userId)
    {
        return $this->getUserRepository()->find($userId);
    }

    /**
     * @param string $email
     * @return \AppBundle\Model\User\User
     */
    public function getByEmail(string $email)
    {
        $user = $this->getUserRepository()->findOneBy(['email' => $email]);

        if ($user === null) {
            throw new UserNotFoundException('User with email "' . $email . '" not found.');
        }

        return $user;
    }
}
