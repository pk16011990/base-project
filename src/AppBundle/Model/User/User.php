<?php

namespace AppBundle\Model\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 */
class User implements UserInterface, \Serializable
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $firstName;

    /**
     * @ORM\Column(type="string" , length=255)
     * @var string
     */
    private $lastName;

    /**
     * @ORM\Column(type="string" , length=255, unique=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=60)
     * @var string
     */
    private $password;

    /**
     * @param \AppBundle\Model\User\UserData $userData
     */
    public function __construct(UserData $userData)
    {
        $this->firstName = $userData->firstName;
        $this->lastName = $userData->lastName;
        $this->email = $userData->email;
        $this->setPassword($userData->password);
    }

    /**
     * @param \AppBundle\Model\User\UserData $userData
     */
    public function edit(UserData $userData)
    {
        $this->firstName = $userData->firstName;
        $this->lastName = $userData->lastName;
        $this->email = $userData->email;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->email,
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->email) = unserialize($serialized);
    }

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return [Role::USER];
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdentificationNumber()
    {
        return '10' . $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
